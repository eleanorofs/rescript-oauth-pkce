# rescript-oauth-pkce

This package was created for personal use. I am not a cryptologist, and I do 
not recommend anyone use any part of this code without an expert's review
and advice. 

Issues and pull requests are open if I did anything wrong here!
