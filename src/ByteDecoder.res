open Js.TypedArray2;

external convertToArrayLike: Uint8Array.t => Js.Array2.t<int> = "%identity";

@val
external btoa: string => string = "btoa"

let base64 = (buffer: ArrayBuffer.t): string =>
    buffer -> Uint8Array.fromBuffer
    -> convertToArrayLike
    -> Js.Array2.reduce((acc, element) => {
      acc ++ Js.String2.fromCharCode(element)
    }, "")
    -> btoa;
