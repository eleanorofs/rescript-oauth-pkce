open Js.TypedArray2;

external convertToArrayLike: Uint8Array.t => Js.Array2.t<int> = "%identity";

@val
external btoa: Uint8Array.t => string = "btoa";

let stringToBuffer = (str: string): ArrayBuffer.t =>
    TextEncoder.make -> TextEncoder.encode(str);

let bytesToString = (b: array<int>): string
  => b -> Js.Array2.map(element => Js.String2.fromCharCode(element))
  -> Js.Array2.joinWith("");

/*let sha256String =
  (window: Dom.window, value: Uint8Array.t): Promise.t<string>
  => window -> Crypto.crypto
  -> Crypto.subtle
  -> SubtleCrypto.digest(#"SHA-256", value -> Uint8Array.buffer)
  -> Promise.thenResolve(buffer => Uint8Array.fromBuffer(buffer))
  -> Promise.thenResolve(bytes => btoa(bytes))
;*/

let sha256 = (window: Dom.window, value: string): Promise.t<string>
    => window -> Crypto.crypto -> Crypto.subtle
    -> SubtleCrypto.digest(#"SHA-256", value -> stringToBuffer)
    -> Promise.thenResolve(bytes => ByteDecoder.base64(bytes));



