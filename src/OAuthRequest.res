open Js.TypedArray2;

@new external makeUint8Array: int => Uint8Array.t = "Uint8Array";

let baseUrl = (protocol: string, host: string): string =>
    protocol -> Js.String2.concat("//") -> Js.String2.concat(host);

let locationBaseUrl = (location: Dom.location): string =>
    location -> Window.Location.protocol
    -> baseUrl(location -> Window.Location.host);

type t = {
  state: string,
  codeVerifier: string,
  codeChallenge: string,
  host: string,
};

let initSync = (challenge: string,
                window: Dom.window,
                state: string,
                verifier: string):
    t => {
      state: state,
      codeVerifier: verifier,
      codeChallenge: challenge,
      host: window -> Window.location -> locationBaseUrl
    };

let init = (verifier: string,
            window: Dom.window,
            state: string):
    Promise.t<t> => {
      window 
        -> Hash.sha256(verifier)
        -> Promise.thenResolve(c => c -> initSync(window, state, verifier));
};

let generate = (window: Dom.window): Promise.t<t> => {
  window -> Crypto.crypto
    -> Crypto.getRandomValues(makeUint8Array(64))
    -> Uint8Array.buffer
    -> ByteDecoder.base64
    -> init(window, window -> Crypto.crypto -> Crypto.randomUUID)
};
