type t;

@send external
digest: (t, Algorithm.t, Js.TypedArray2.ArrayBuffer.t)
  => Js.Promise.t<Js.TypedArray2.ArrayBuffer.t>
  = "digest";




