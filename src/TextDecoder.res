type t;

@new external make: () => t = "TextDecoder";

@send external decode: (t, Js.TypedArray2.ArrayBuffer.t) => string = "decode";
