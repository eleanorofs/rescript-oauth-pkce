type t;

@new external make: t = "TextEncoder";

@send external encode: (t, string) => Js.TypedArray2.ArrayBuffer.t = "encode";

//TODO this binding is incomplete. 

