@val external window: Dom.window = "window";

@get external location: Dom.window => Dom.location = "location";

@send external btoa: (Dom.window, string) => string = "btoa";

module Location = {
  @get external host: Dom.location => string = "host";
  
  @get external protocol: Dom.location => string = "protocol";
};
