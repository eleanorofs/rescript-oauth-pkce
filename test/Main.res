@val external window: Dom.window = "window";

@val external atob: string => string = "atob";

let _ = OAuthRequest.generate(window)
    -> Promise.thenResolve(req => Js.Console.log(req));

let plain: string = "ks02i3jdikdo2k0dkfodf3m39rjfjsdk0wk349rj3jrhf";

let hashPromise: Promise.t<string> = Hash.sha256(window, plain);

Js.Console.info("Hash of example:");
let _ = hashPromise -> Promise.thenResolve(str => Js.Console.info(str));

